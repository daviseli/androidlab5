package cs.mad.flashcards.activities

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.room.Room
import cs.mad.flashcards.adapters.FlashcardAdapter
import cs.mad.flashcards.adapters.FlashcardSetAdapter
import cs.mad.flashcards.databinding.ActivityFlashcardSetDetailBinding
import cs.mad.flashcards.entities.FlashcardDatabase
import cs.mad.flashcards.entities.FlashcardEntity
import cs.mad.flashcards.entities.FlashcardEntityDao
import cs.mad.flashcards.entities.FlashcardSetEntityDao
import cs.mad.flashcards.runOnIO

class FlashcardSetDetailActivity : AppCompatActivity() {
    private lateinit var binding: ActivityFlashcardSetDetailBinding
    private lateinit var prefs: SharedPreferences
    private lateinit var flashcardDao: FlashcardEntityDao
    private var list: List<FlashcardEntity> = emptyList()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFlashcardSetDetailBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        val db = Room.databaseBuilder(
            applicationContext,
            FlashcardDatabase::class.java, FlashcardDatabase.databaseName
        ).fallbackToDestructiveMigration().build()

        flashcardDao = db.fcardDao()

        prefs = getSharedPreferences("flashcardPrefs", MODE_PRIVATE)

        binding.flashcardList.adapter = FlashcardAdapter(list, flashcardDao)

        binding.addFlashcardButton.setOnClickListener {
            (binding.flashcardList.adapter as FlashcardAdapter).addItem(FlashcardEntity(0, "test", "test"))
            binding.flashcardList.smoothScrollToPosition((binding.flashcardList.adapter as FlashcardAdapter).itemCount - 1)
        }

        binding.studySetButton.setOnClickListener {
            startActivity(Intent(this, StudySetActivity::class.java))
        }

        binding.deleteSetButton.setOnClickListener {
            finish()
        }

        //clearStorage()
        loadData()
    }

    private fun clearStorage() {
        runOnIO {
            flashcardDao.deleteAll()
        }
    }

    private fun loadData() {
        runOnIO{ (binding.flashcardList.adapter as FlashcardAdapter).update(flashcardDao.getAll()) }
    }
}