package cs.mad.flashcards.entities

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = arrayOf(FlashcardEntity::class, FlashcardSetEntity::class), version = 4)
abstract class FlashcardDatabase: RoomDatabase() {
    companion object{
        const val databaseName = "FCARDDATABASE"
    }

    abstract fun fcardDao() : FlashcardEntityDao
    abstract fun fcardSetDao() : FlashcardSetEntityDao
}