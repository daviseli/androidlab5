package cs.mad.flashcards.entities

import androidx.room.*
import java.time.zone.ZoneOffsetTransitionRule

data class FlashcardEntityContainer (
    val flashcards: List<FlashcardEntity>
)

data class FlashcardSetEntityContainer (
    val flashcardSets: List<FlashcardSetEntity>
)

@Entity
data class FlashcardEntity(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    var question: String,
    var answer: String
)

@Entity
data class FlashcardSetEntity(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    var title: String
)

@Dao
interface FlashcardEntityDao {
    @Query("select * from flashcardentity")
    suspend fun getAll(): List<FlashcardEntity>

    @Insert
    suspend fun insert(flash: FlashcardEntity)

    @Insert
    suspend fun insertAll(flashes: List<FlashcardEntity>)

    @Update
    suspend fun update(flash: FlashcardEntity)

    @Delete
    suspend fun delete(flash: FlashcardEntity)

    @Query("delete from flashcardentity")
    suspend fun deleteAll()
}

@Dao
interface FlashcardSetEntityDao {
    @Query("select * from flashcardsetentity")
    suspend fun getAll(): List<FlashcardSetEntity>

    @Insert
    suspend fun insert(flashSet: FlashcardSetEntity)

    @Insert
    suspend fun insertAll(flashSets: List<FlashcardSetEntity>)

    @Update
    suspend fun update(flashSet: FlashcardSetEntity)

    @Delete
    suspend fun delete(flashSet: FlashcardSetEntity)

    @Query("delete from flashcardsetentity")
    suspend fun deleteAll()
}