package cs.mad.flashcards.activities

import android.content.SharedPreferences
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.room.Room
import cs.mad.flashcards.adapters.FlashcardSetAdapter
import cs.mad.flashcards.databinding.ActivityMainBinding
import cs.mad.flashcards.entities.*
import cs.mad.flashcards.runOnIO

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var  prefs: SharedPreferences
    private lateinit var flashcardDao: FlashcardEntityDao
    private lateinit var flashcardSetDao: FlashcardSetEntityDao
    private var list: List<FlashcardSetEntity> = emptyList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val db = Room.databaseBuilder(
            applicationContext,
            FlashcardDatabase::class.java, FlashcardDatabase.databaseName
        ).fallbackToDestructiveMigration().build()

        flashcardSetDao = db.fcardSetDao()

        prefs = getSharedPreferences("flashcardPrefs", MODE_PRIVATE)

        binding.flashcardSetList.adapter = FlashcardSetAdapter(list, flashcardSetDao)

        binding.createSetButton.setOnClickListener {
            (binding.flashcardSetList.adapter as FlashcardSetAdapter).addItem(FlashcardSetEntity(0, "test"))
            binding.flashcardSetList.smoothScrollToPosition((binding.flashcardSetList.adapter as FlashcardSetAdapter).itemCount - 1)
        }

        //clearStorage()
        loadData()
    }

    private fun clearStorage() {
        runOnIO {
            flashcardSetDao.deleteAll()
        }
    }

    private fun loadData() {
        runOnIO{ (binding.flashcardSetList.adapter as FlashcardSetAdapter).update(flashcardSetDao.getAll()) }
    }
}