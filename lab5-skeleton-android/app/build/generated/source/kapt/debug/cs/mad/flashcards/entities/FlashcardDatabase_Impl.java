package cs.mad.flashcards.entities;

import androidx.annotation.NonNull;
import androidx.room.DatabaseConfiguration;
import androidx.room.InvalidationTracker;
import androidx.room.RoomOpenHelper;
import androidx.room.RoomOpenHelper.Delegate;
import androidx.room.RoomOpenHelper.ValidationResult;
import androidx.room.migration.AutoMigrationSpec;
import androidx.room.migration.Migration;
import androidx.room.util.DBUtil;
import androidx.room.util.TableInfo;
import androidx.room.util.TableInfo.Column;
import androidx.room.util.TableInfo.ForeignKey;
import androidx.room.util.TableInfo.Index;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.sqlite.db.SupportSQLiteOpenHelper;
import androidx.sqlite.db.SupportSQLiteOpenHelper.Callback;
import androidx.sqlite.db.SupportSQLiteOpenHelper.Configuration;
import java.lang.Class;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@SuppressWarnings({"unchecked", "deprecation"})
public final class FlashcardDatabase_Impl extends FlashcardDatabase {
  private volatile FlashcardEntityDao _flashcardEntityDao;

  private volatile FlashcardSetEntityDao _flashcardSetEntityDao;

  @Override
  protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration configuration) {
    final SupportSQLiteOpenHelper.Callback _openCallback = new RoomOpenHelper(configuration, new RoomOpenHelper.Delegate(4) {
      @Override
      public void createAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("CREATE TABLE IF NOT EXISTS `FlashcardEntity` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `question` TEXT NOT NULL, `answer` TEXT NOT NULL)");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `FlashcardSetEntity` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `title` TEXT NOT NULL)");
        _db.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
        _db.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, 'b48323156042d599050c8d91b1e3633b')");
      }

      @Override
      public void dropAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("DROP TABLE IF EXISTS `FlashcardEntity`");
        _db.execSQL("DROP TABLE IF EXISTS `FlashcardSetEntity`");
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onDestructiveMigration(_db);
          }
        }
      }

      @Override
      protected void onCreate(SupportSQLiteDatabase _db) {
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onCreate(_db);
          }
        }
      }

      @Override
      public void onOpen(SupportSQLiteDatabase _db) {
        mDatabase = _db;
        internalInitInvalidationTracker(_db);
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onOpen(_db);
          }
        }
      }

      @Override
      public void onPreMigrate(SupportSQLiteDatabase _db) {
        DBUtil.dropFtsSyncTriggers(_db);
      }

      @Override
      public void onPostMigrate(SupportSQLiteDatabase _db) {
      }

      @Override
      protected RoomOpenHelper.ValidationResult onValidateSchema(SupportSQLiteDatabase _db) {
        final HashMap<String, TableInfo.Column> _columnsFlashcardEntity = new HashMap<String, TableInfo.Column>(3);
        _columnsFlashcardEntity.put("id", new TableInfo.Column("id", "INTEGER", true, 1, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsFlashcardEntity.put("question", new TableInfo.Column("question", "TEXT", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsFlashcardEntity.put("answer", new TableInfo.Column("answer", "TEXT", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        final HashSet<TableInfo.ForeignKey> _foreignKeysFlashcardEntity = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesFlashcardEntity = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoFlashcardEntity = new TableInfo("FlashcardEntity", _columnsFlashcardEntity, _foreignKeysFlashcardEntity, _indicesFlashcardEntity);
        final TableInfo _existingFlashcardEntity = TableInfo.read(_db, "FlashcardEntity");
        if (! _infoFlashcardEntity.equals(_existingFlashcardEntity)) {
          return new RoomOpenHelper.ValidationResult(false, "FlashcardEntity(cs.mad.flashcards.entities.FlashcardEntity).\n"
                  + " Expected:\n" + _infoFlashcardEntity + "\n"
                  + " Found:\n" + _existingFlashcardEntity);
        }
        final HashMap<String, TableInfo.Column> _columnsFlashcardSetEntity = new HashMap<String, TableInfo.Column>(2);
        _columnsFlashcardSetEntity.put("id", new TableInfo.Column("id", "INTEGER", true, 1, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsFlashcardSetEntity.put("title", new TableInfo.Column("title", "TEXT", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        final HashSet<TableInfo.ForeignKey> _foreignKeysFlashcardSetEntity = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesFlashcardSetEntity = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoFlashcardSetEntity = new TableInfo("FlashcardSetEntity", _columnsFlashcardSetEntity, _foreignKeysFlashcardSetEntity, _indicesFlashcardSetEntity);
        final TableInfo _existingFlashcardSetEntity = TableInfo.read(_db, "FlashcardSetEntity");
        if (! _infoFlashcardSetEntity.equals(_existingFlashcardSetEntity)) {
          return new RoomOpenHelper.ValidationResult(false, "FlashcardSetEntity(cs.mad.flashcards.entities.FlashcardSetEntity).\n"
                  + " Expected:\n" + _infoFlashcardSetEntity + "\n"
                  + " Found:\n" + _existingFlashcardSetEntity);
        }
        return new RoomOpenHelper.ValidationResult(true, null);
      }
    }, "b48323156042d599050c8d91b1e3633b", "9f7c921339471033faffc550e8159777");
    final SupportSQLiteOpenHelper.Configuration _sqliteConfig = SupportSQLiteOpenHelper.Configuration.builder(configuration.context)
        .name(configuration.name)
        .callback(_openCallback)
        .build();
    final SupportSQLiteOpenHelper _helper = configuration.sqliteOpenHelperFactory.create(_sqliteConfig);
    return _helper;
  }

  @Override
  protected InvalidationTracker createInvalidationTracker() {
    final HashMap<String, String> _shadowTablesMap = new HashMap<String, String>(0);
    HashMap<String, Set<String>> _viewTables = new HashMap<String, Set<String>>(0);
    return new InvalidationTracker(this, _shadowTablesMap, _viewTables, "FlashcardEntity","FlashcardSetEntity");
  }

  @Override
  public void clearAllTables() {
    super.assertNotMainThread();
    final SupportSQLiteDatabase _db = super.getOpenHelper().getWritableDatabase();
    try {
      super.beginTransaction();
      _db.execSQL("DELETE FROM `FlashcardEntity`");
      _db.execSQL("DELETE FROM `FlashcardSetEntity`");
      super.setTransactionSuccessful();
    } finally {
      super.endTransaction();
      _db.query("PRAGMA wal_checkpoint(FULL)").close();
      if (!_db.inTransaction()) {
        _db.execSQL("VACUUM");
      }
    }
  }

  @Override
  protected Map<Class<?>, List<Class<?>>> getRequiredTypeConverters() {
    final HashMap<Class<?>, List<Class<?>>> _typeConvertersMap = new HashMap<Class<?>, List<Class<?>>>();
    _typeConvertersMap.put(FlashcardEntityDao.class, FlashcardEntityDao_Impl.getRequiredConverters());
    _typeConvertersMap.put(FlashcardSetEntityDao.class, FlashcardSetEntityDao_Impl.getRequiredConverters());
    return _typeConvertersMap;
  }

  @Override
  public Set<Class<? extends AutoMigrationSpec>> getRequiredAutoMigrationSpecs() {
    final HashSet<Class<? extends AutoMigrationSpec>> _autoMigrationSpecsSet = new HashSet<Class<? extends AutoMigrationSpec>>();
    return _autoMigrationSpecsSet;
  }

  @Override
  public List<Migration> getAutoMigrations(
      @NonNull Map<Class<? extends AutoMigrationSpec>, AutoMigrationSpec> autoMigrationSpecsMap) {
    return Arrays.asList();
  }

  @Override
  public FlashcardEntityDao fcardDao() {
    if (_flashcardEntityDao != null) {
      return _flashcardEntityDao;
    } else {
      synchronized(this) {
        if(_flashcardEntityDao == null) {
          _flashcardEntityDao = new FlashcardEntityDao_Impl(this);
        }
        return _flashcardEntityDao;
      }
    }
  }

  @Override
  public FlashcardSetEntityDao fcardSetDao() {
    if (_flashcardSetEntityDao != null) {
      return _flashcardSetEntityDao;
    } else {
      synchronized(this) {
        if(_flashcardSetEntityDao == null) {
          _flashcardSetEntityDao = new FlashcardSetEntityDao_Impl(this);
        }
        return _flashcardSetEntityDao;
      }
    }
  }
}
